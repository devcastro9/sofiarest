# sofiaREST

Este el backend del sistema sofiaREST.

Stack utilizado:
    
    1. BackEnd:

        * Lenguaje de programacion backend: C# v10

        * SDK: .NET6 v6.0.202 (version LTS vigente hasta el 12-Nov-2024)

        * ORM: Entity Framework Core

        * Base de datos: Microsoft SQL Server 2017/2016/2014

    2. FrontEnd

        * Manejador de versiones de NodeJS: NVM

        * NodeJS Gallium v16.15.0 (version LTS vigente hasta el 30-Abr-2024)

        * NPM v8.5.5
        
        * Angular CLI: ng v13.3.4  

    3. Caracteristicas de servidor de pruebas

        * Windows Server 2016

        * Servidor web: IIS

## Uso de ORM con la base de datos

Suponiendo que la base de datos se encuentra en la red local, en la IP privada: 192.168.3.131 bajo el nombre "FacturacionDB", las cadenas de conexion serian de esta forma. En caso de usar soluciones de base de datos Sql Server en Azure/AWS, esta corresponderia a la IP publica previa configuracion para que sea accesible por la aplicacion.

El ORM EF Core tiene dos metodos segun el origen de datos:

    1. Code First -> Migraciones
    2. Database First -> Ingenieria inversa

Dado que la administracion de la base de datos con codigo (Code First) en aplicaciones extensas se torna compleja, se usara el enfoque "Database first".

### Usando ingenieria inversa en la base de datos por primera vez

Consola en Visual Studio:

`Scaffold-DbContext 'Data Source=192.168.3.131;Initial Catalog=FacturacionDB;Trusted_Connection=True' Microsoft.EntityFrameworkCore.SqlServer -DataAnnotations -ContextDir Data -OutputDir Models`

Terminal:

`dotnet ef dbcontext scaffold "Data Source=192.168.3.131;Initial Catalog=FacturacionDB;Trusted_Connection=True" Microsoft.EntityFrameworkCore.SqlServer --data-annotations --context-dir Data --output-dir Models`

Conservando los nombres de la base de datos sin tomar en cuenta las convenciones .NET, por consola de Visual Studio:

`Scaffold-DbContext 'Data Source=192.168.3.131;Initial Catalog=FacturacionDB;Trusted_Connection=True' Microsoft.EntityFrameworkCore.SqlServer -UseDatabaseNames -DataAnnotations -ContextDir Data -OutputDir Models`

Conservando los nombres de la base de datos sin tomar en cuenta las convenciones .NET, por terminal:

`dotnet ef dbcontext scaffold "Data Source=192.168.3.131;Database=FacturacionDB;Trusted_Connection=Yes" Microsoft.EntityFrameworkCore.SqlServer --use-database-names --data-annotations --context-dir Data --output-dir Models`

### Ingenieria inversa cuando ya existe un mapeo previo

Tomar en cuenta que **sobreescribira** todos los cambios en los modelos, incluyendo cambios propios.

Consola en Visual Studio:

`Scaffold-DbContext 'Data Source=192.168.3.131;Initial Catalog=FacturacionDB;Trusted_Connection=True' Microsoft.EntityFrameworkCore.SqlServer -DataAnnotations -ContextDir Data -OutputDir Models -Force`

Terminal:

`dotnet ef dbcontext scaffold "Data Source=192.168.3.131;Database=FacturacionDB;Trusted_Connection=Yes" Microsoft.EntityFrameworkCore.SqlServer --data-annotations --context-dir Data --output-dir Models --force`

### Actualizacion de los modelos de la base de datos

Comando nuevo que mantiene los cambios personalizados en los modelos, solo toma en cuenta los estan en el Data/DbContext.cs

Consola de Visual Studio:

`Update-DbContext`

Terminal:

`dotnet ef dbcontext update`

### Creacion de modelos segun tablas especificas

Consola en Visual Studio:

`Scaffold-DbContext 'Data Source=192.168.3.131;Initial Catalog=FacturacionDB;Trusted_Connection=True' Microsoft.EntityFrameworkCore.SqlServer -DataAnnotations -ContextDir Data -OutputDir Models -Tables tblAlgo, tblAlgo2`

Terminal:

`dotnet ef dbcontext scaffold "Data Source=192.168.3.131;Initial Catalog=FacturacionDB;Trusted_Connection=True" Microsoft.EntityFrameworkCore.SqlServer --data-annotations --context-dir Data --output-dir Models --table tblAlgo --table tblAlgo2`
